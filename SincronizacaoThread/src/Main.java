
public class Main {

	public static void main(String[] args) {
		
		Impressora i1 = new Impressora();
		
		ThreadImpressora ti1 = new ThreadImpressora(i1);
		ThreadImpressora ti2 = new ThreadImpressora(i1);
		ti1.start();
		ti2.start();
	}

}
