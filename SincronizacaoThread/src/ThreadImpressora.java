
public class ThreadImpressora extends Thread{
	
	Impressora imp;
	
	public ThreadImpressora(Impressora imp){
		this.imp = imp;
	}
	
	public void run(){
		imp.imprimir();
	}
}
