package main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.Normalizer;
import java.util.HashMap;

public class Main {

	public static void main(String[] args) {
		long inicio = System.currentTimeMillis();
		File diretorio = new File("C:\\www\\Prog_V\\Atividade1\\Producao");
		HashMap<Character, Integer> dic = new HashMap<Character, Integer>();
		File [] arquivos = diretorio.listFiles();
		String linha;
		FileReader arq;
		BufferedReader lerArq;
		for(File a : arquivos){
			try {
				arq = new FileReader(a);
				lerArq = new BufferedReader(arq);
				linha = lerArq.readLine();
				
				while (lerArq.readLine() != null) {
					linha = linha.replaceAll("[^a-zA-Z\u00C0-\u00FF ]", "");
					linha = semAcento(linha).toLowerCase();
					for(char letra : linha.toCharArray()){
						Character key = new Character(letra);
						if (!dic.containsKey(key)) {
							dic.put(key, 0);
						}
						Integer numero = dic.get(key);
						dic.put(key, new Integer(numero.intValue()+1));
				    }
					linha = lerArq.readLine();
				}
				 
			    arq.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
		for (Character character : dic.keySet()) {
			System.out.printf("%c : %d \n", character, dic.get(character));
		}
		
		long fim  = System.currentTimeMillis();  
		System.out.println( fim - inicio );
		
	}
	
	public static String semAcento(String str) {
		return Normalizer.normalize(str, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
    }

}
