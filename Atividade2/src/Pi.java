
public class Pi {
	private double soma;
	private double inicio;
	private double fim;
	
	public Pi(double inicio, double fim){
		this.soma = 0;
		this.inicio = inicio;
		this.fim = fim;
	}
	
	public double getSoma(){
		for(double n = inicio; n <= fim; n++){
			if(n % 2 == 0)
				soma += 1 / (2*n + 1);
			else
				soma += (-1) / (2*n + 1);
		}
		return this.soma * 4;
	}
	
}
