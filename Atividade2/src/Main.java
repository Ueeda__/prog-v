import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {
		long inicio = System.currentTimeMillis();
		int numeroThreads = 2;
		double parcela = 1000000000.0 / numeroThreads;
		System.out.println(parcela);
		ArrayList<PiThread> threads = new ArrayList<PiThread>(numeroThreads);
		for(int i = 0; i < numeroThreads; i++){
			double init = (i > 0) ? (parcela + 1) : 0;
			double end = (i > 0) ? (parcela * i) : parcela;
			
			threads.add(new PiThread(init, end));
		}
		for(PiThread thread : threads){
			thread.start();
		}
		try {
			for(PiThread thread : threads){
				thread.join();
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		long fim  = System.currentTimeMillis();
		System.out.println( fim - inicio );
	}

}
