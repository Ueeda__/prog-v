
public class PiThread extends Thread{
	private Pi pi;
	
	public PiThread(double d, double fim){
		this.pi = new Pi(d, fim);
	}
	
	public void run(){
		System.out.println(pi.getSoma());
	}
	
	public Pi getPi(){
		return this.pi;
	}
}
