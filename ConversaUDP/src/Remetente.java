import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class Remetente {
	public static void main(String[] args) throws IOException {
		DatagramSocket socket;		
		socket = new DatagramSocket();
		InetAddress addr = InetAddress.getLocalHost();
		String pergunta = "Qual a hora?";
		byte[] perguntaBytes = pergunta.getBytes();
		DatagramPacket msgEnviada = new DatagramPacket(perguntaBytes, perguntaBytes.length, addr, 3333);
		socket.send(msgEnviada);
//		DatagramPacket msgRecebida = new DatagramPacket(new byte[1024], 1024);
//		socket.receive(msgRecebida);
//		String msgRecebidaString = new String(msgRecebida.getData(), msgRecebida.getOffset(),msgRecebida.getLength());
//		System.out.println(msgRecebidaString);
		socket.close();
	}
}
