package Structures;

import java.util.ArrayList;

import Threads.ProdutorThread;

public class Fila {
	ArrayList<Integer> fila;
	
	public Fila(){
		this.fila = new ArrayList<Integer>(10);
	}
	
	public synchronized void insere(int valor){
		while(isFull()){
			try {
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		this.fila.add(valor);		
		notifyAll();
	}
	
	public synchronized int remove(){
		while(vazia()){
			try {
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return this.fila.remove(0);
	}
	
	public boolean vazia(){
		return this.fila.size() == 0;
	}
	
	public boolean isFull(){
		return this.fila.size() >= 10;
	}
	
	public int size(){
		return this.fila.size();
	}
	
}
