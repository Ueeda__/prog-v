package Models;

public class Pessoa {
	private int id;
	
	public Pessoa(){}
	
	public Pessoa(int id){
		this.id = id;
	}
	
	public int getId(){
		return this.id;
	}
	
	public void setId(int id){
		this.id = id;
	}
	
}
