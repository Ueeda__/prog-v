package Models;
import java.util.Random;

public class Produtor extends Pessoa{
	
	public Produtor(int id){
		super(id);
	}
	
	public int gerarValor(){
		Random gerador = new Random();
		return gerador.nextInt(30);
	}
	
	public int getId(){
		return super.getId();
	}
	
	public void setId(int id){
		super.setId(id);
	}
	
}
