package Models;
public class Consumidor extends Pessoa{
	
//	private int[] retiradas;
	
	public Consumidor(int id){
		super(id);
	}	
	
	public synchronized void armazenarRetirada(int valor){
		System.out.printf("O consumidor %d tirou o valor %d \n", this.getId(), valor);
			
	}
	
	public int getId(){
		return super.getId();
	}
	
	public void setId(int id){
		super.setId(id);
	}
}
