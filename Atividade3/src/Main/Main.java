package Main;

import java.util.ArrayList;

import Structures.Fila;
import Threads.*;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Fila fila = new Fila();
		ArrayList<ConsumidorThread> consumidores = new ArrayList<ConsumidorThread>();
		ArrayList<ProdutorThread> produtores = new ArrayList<ProdutorThread>();
		for(int i = 0; i < 3; i++){
			produtores.add(new ProdutorThread(fila, (i + 1)));
		}
		for(int i = 0; i < 2; i++){
			consumidores.add(new ConsumidorThread(fila, (i + 1)));
		}
	    
    	for(ProdutorThread p : produtores)
			p.start();
		
   
		for(ConsumidorThread c : consumidores)
			c.start();
	}

}
