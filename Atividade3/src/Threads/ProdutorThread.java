package Threads;

import Models.Produtor;
import Structures.Fila;

public class ProdutorThread extends Thread{
	private Produtor produtor;
	private Fila filaPrincipal;
	
	public ProdutorThread(Fila filaPrincipal, int id){
		this.filaPrincipal = filaPrincipal;
		this.produtor = new Produtor(id);
	}
	
	public void run(){
//		synchronized (filaPrincipal) {
			while(true){
				int valor = produtor.gerarValor();
				this.filaPrincipal.insere(valor);
				System.out.printf("Produtor %d inseriu o valor %d \n", produtor.getId(), valor);
				
				try {
					sleep(300);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
//		}		
	}
}
