package Threads;

import Models.Consumidor;
import Structures.Fila;

public class ConsumidorThread extends Thread{
	private Consumidor consumidor;
	private Fila filaPrincipal;
	
	public ConsumidorThread(Fila filaPrincipal, int id){
		this.filaPrincipal = filaPrincipal;
		this.consumidor = new Consumidor(id);
	}
	
	public void run(){
		while(true){
			consumidor.armazenarRetirada(filaPrincipal.remove());
			try {
				sleep(20000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
