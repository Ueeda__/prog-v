import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class Atendente extends Thread{
	
	private Socket socket;
	
	public Atendente(Socket socket){
		this.socket = socket;
	}
	
	public void run(){
		try{
			DataInputStream in = new DataInputStream(this.socket.getInputStream());
			DataOutputStream out = new DataOutputStream(this.socket.getOutputStream());
			
			
			double altura = in.readDouble();
			double peso = in.readDouble();
			double imc = peso/(altura*altura);
			
			out.writeDouble(imc);
			
			in.close();
			out.close();
			socket.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
