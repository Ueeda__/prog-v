import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

public class Cliente {

	public static void main(String[] args) throws UnknownHostException, IOException {
		// TODO Auto-generated method stub
		Socket socket = new Socket("10.96.26.60", 3333);
//		Socket socket = new Socket("localhost", 3333);
		DataInputStream in = new DataInputStream(socket.getInputStream());
		DataOutputStream out = new DataOutputStream(socket.getOutputStream());
		
		out.writeDouble(1.95);
		out.writeDouble(144);
		
		double imc = in.readDouble();
		
		System.out.println(imc);
		
		in.close();
		out.close();
		socket.close();
	}

}
